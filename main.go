package main

import (
	"fmt"
	"os"

	"gitlab.com/code_tests/mars_rover.git/models"
	"gitlab.com/code_tests/mars_rover.git/parser"
)

func main() {
	grid, robots, commandLists, err := parser.ParseInput(os.Args[1])
	if err != nil {
		panic(err)
	}
	for i, robot := range robots {
		commands := commandLists[i]
		movements:
		for _, command := range commands {
			switch command {
			case models.Left:
				robot.Orientation = robot.LeftwardOrientation()
			case models.Right:
				robot.Orientation = robot.RightwardOrientation()
			case models.Forward:
				newPosition := robot.ForwardPosition()
				if grid.IsInGrid(newPosition) {
					robot.Position = newPosition
				} else {
					robot.Lost = true
					break movements
				}
			default:
				panic("unknown command")
			}
		}
		fmt.Println(robot)
	}
}
