Run with, for example, `go run main.go "4 8\n(2, 3, E) LFRFF\n(0, 2, N) FFLFRFF"` in the root directory.

Test with `go test`, in the `test` folder.