package models

type Grid struct {
	MaxX int
	MaxY int
}

func (g *Grid) IsInGrid(position Position) bool {
	return position.X >= 0 && position.Y >= 0 && position.X <= g.MaxX && position.Y <= g.MaxY
}
