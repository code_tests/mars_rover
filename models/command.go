package models

type Command string

const (
	Left    Command = "L"
	Right   Command = "R"
	Forward Command = "F"
)
