package models

import "fmt"

type Robot struct {
	Position    Position
	Orientation Orientation
	Lost        bool
}

func (r *Robot) LeftwardOrientation() (newOrientation Orientation) {
	switch r.Orientation {
	case North:
		newOrientation = West
	case East:
		newOrientation = North
	case South:
		newOrientation = East
	case West:
		newOrientation = South
	default:
		panic("unknown orientation")
	}
	return
}

func (r *Robot) RightwardOrientation() (newOrientation Orientation) {
	switch r.Orientation {
	case North:
		newOrientation = East
	case East:
		newOrientation = South
	case South:
		newOrientation = West
	case West:
		newOrientation = North
	default:
		panic("unknown orientation")
	}
	return
}

func (r *Robot) ForwardPosition() (newPosition Position) {
	switch r.Orientation {
	case North:
		newPosition = Position{
			X: r.Position.X,
			Y: r.Position.Y + 1,
		}
	case East:
		newPosition = Position{
			X: r.Position.X + 1,
			Y: r.Position.Y,
		}
	case South:
		newPosition = Position{
			X: r.Position.X,
			Y: r.Position.Y - 1,
		}
	case West:
		newPosition = Position{
			X: r.Position.X - 1,
			Y: r.Position.Y,
		}
	default:
		panic("unknown orientation")
	}
	return
}

func (r Robot) String() string {
	position := fmt.Sprintf("(%d, %d, %v)", r.Position.X, r.Position.Y, r.Orientation)
	if r.Lost {
		position += " LOST"
	}
	return position
}
