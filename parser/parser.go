package parser

import (
	"strconv"
	"strings"

	"gitlab.com/code_tests/mars_rover.git/models"
)

func ParseInput(input string) (grid models.Grid, robots []models.Robot, commands [][]models.Command, err error) {
	lines := strings.Split(input, "\\n")
	
	gridParams := strings.Split(lines[0], " ")
	grid.MaxX, err = strconv.Atoi(gridParams[0])
	if err != nil {
		return
	}
	grid.MaxY, err = strconv.Atoi(gridParams[1])
	if err != nil {
		return
	}

	for _, line := range lines[1:] {
		robotParams := strings.Split(line, ") ")
		statusString := strings.ReplaceAll(robotParams[0], "(", "")
		robotStatus := strings.Split(statusString, ", ")
		x, err := strconv.Atoi(robotStatus[0])
		if err != nil {
			return grid, robots, commands, err
		}
		y, err := strconv.Atoi(robotStatus[1])
		if err != nil {
			return grid, robots, commands, err
		}
		robots = append(robots, models.Robot{
			Position: models.Position{
				X: x,
				Y: y,
			},
			Orientation: models.Orientation(robotStatus[2]),
			Lost: false,
		})

		var newCommands []models.Command
		for _, commandString := range strings.Split(robotParams[1], "") {
			newCommands = append(newCommands, models.Command(commandString))
		}
		commands = append(commands, newCommands)
	}

	return
}