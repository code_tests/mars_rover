package test

import (
	"testing"

	"gitlab.com/code_tests/mars_rover.git/models"
)

func TestIsInGrid(t *testing.T) {
	tables := []struct {
		maxX     int
		maxY     int
		x        int
		y        int
		expected bool
	}{
		{0, 0, 1, 2, false},
		{10, 10, 5, 5, true},
		{5, 5, -4, 2, false},
		{5, 5, 8, 3, false},
		{10, 10, 10, 5, true},
		{10, 10, 0, 0, true},
	}

	for _, table := range tables {
		grid := models.Grid{
			MaxX: table.maxX,
			MaxY: table.maxY,
		}
		position := models.Position{
			X: table.x,
			Y: table.y,
		}
		if grid.IsInGrid(position) != table.expected {
			t.Errorf("IsInGrid did not return %t, for grid [%d,%d] and coordinate (%d,%d)",
				table.expected,
				table.maxX,
				table.maxY,
				table.x,
				table.y,
			)
		}
	}
}
