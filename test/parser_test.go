package test

import (
	"testing"

	"gitlab.com/code_tests/mars_rover.git/parser"
)

func TestParseInput(t *testing.T) {
	grid, robots, commands, err := parser.ParseInput("4 8\\n(2, 3, E) LFRFF\\n(0, 2, N) FFLFRFF")
	if err != nil {
		t.Errorf("Expected no error, error: %v", err)
	}

	if grid.MaxX != 4 {
		t.Errorf("Expected grid.MaxX to be 4, was: %d", grid.MaxX)
	}
	if grid.MaxY != 8 {
		t.Errorf("Expected grid.MaxY to be 8, was: %d", grid.MaxY)
	}

	if len(robots) != 2 {
		t.Errorf("Expected 2 robots, got: %d", len(robots))
	}
	if robots[0].Position.X != 2 {
		t.Errorf("Expected robot 1 to have x=2, got: %d", robots[0].Position.X)
	}
	if robots[0].Position.Y != 3 {
		t.Errorf("Expected robot 1 to have y=3, got: %d", robots[0].Position.Y)
	}
	if robots[0].Orientation != "E" {
		t.Errorf("Expected robot 1 be facing E, got: %v", robots[0].Orientation)
	}
	if robots[1].Position.X != 0 {
		t.Errorf("Expected robot 2 to have x=0, got: %d", robots[1].Position.X)
	}
	if robots[1].Position.Y != 2 {
		t.Errorf("Expected robot 2 to have y=2, got: %d", robots[1].Position.Y)
	}
	if robots[1].Orientation != "N" {
		t.Errorf("Expected robot 1 be facing N, got: %v", robots[1].Orientation)
	}

	if len(commands) != len(robots) {
		t.Errorf("Expected robots and commands to be the same length, instead, robots: %d, commands: %d", len(robots), len(commands))
	}
	if commands[0][0] != "L" {
		t.Errorf("Expected first command to be L, was %v", commands[0][0])
	}
	if commands[1][6] != "F" {
		t.Errorf("Expected last command to be F, was %v", commands[0][0])
	}
}
