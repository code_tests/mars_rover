package test

import (
	"testing"

	"gitlab.com/code_tests/mars_rover.git/models"
)

func TestLeftwardOrientation(t *testing.T) {
	tables := []struct {
		input    models.Orientation
		expected models.Orientation
	}{
		{"N", "W"},
		{"W", "S"},
		{"S", "E"},
		{"E", "N"},
	}

	for _, table := range tables {
		robot := models.Robot{
			Position: models.Position{
				X: 0,
				Y: 0,
			},
			Lost:        false,
			Orientation: models.Orientation(table.input),
		}
		actual := robot.LeftwardOrientation()

		if actual != table.expected {
			t.Errorf("Leftward result was incorrect, expected %v, actual %v", table.expected, actual)
		}
	}
}

func TestRightwardOrientation(t *testing.T) {
	tables := []struct {
		input    models.Orientation
		expected models.Orientation
	}{
		{"N", "E"},
		{"W", "N"},
		{"S", "W"},
		{"E", "S"},
	}

	for _, table := range tables {
		robot := models.Robot{
			Position: models.Position{
				X: 0,
				Y: 0,
			},
			Lost:        false,
			Orientation: models.Orientation(table.input),
		}
		actual := robot.RightwardOrientation()

		if actual != table.expected {
			t.Errorf("Rightward result was incorrect, expected %v, actual %v", table.expected, actual)
		}
	}
}

func TestForwardPosition(t *testing.T) {
	tables := []struct {
		inputX      int
		inputY      int
		orientation models.Orientation
		expectedX   int
		expectedY   int
	}{
		{0, 0, "N", 0, 1},
		{4, 7, "E", 5, 7},
		{1, 2, "S", 1, 1},
		{3, 9, "W", 2, 9},
	}

	for _, table := range tables {
		robot := models.Robot{
			Position: models.Position{
				X: table.inputX,
				Y: table.inputY,
			},
			Lost:        false,
			Orientation: models.Orientation(table.orientation),
		}
		actual := robot.ForwardPosition()

		if actual.X != table.expectedX || actual.Y != table.expectedY {
			t.Errorf("Forward position was incorrect, expected (%d,%d), actual (%d,%d)",
				table.expectedX,
				table.expectedY,
				actual.X,
				actual.Y,
			)
		}
	}
}

func TestString(t *testing.T) {
	tables := []struct {
		inputX      int
		inputY      int
		orientation models.Orientation
		lost        bool
		expected    string
	}{
		{0, 0, "N", false, "(0, 0, N)"},
		{4, 7, "E", true, "(4, 7, E) LOST"},
		{1, 2, "S", false, "(1, 2, S)"},
		{3, 9, "W", true, "(3, 9, W) LOST"},
	}

	for _, table := range tables {
		robot := models.Robot{
			Position: models.Position{
				X: table.inputX,
				Y: table.inputY,
			},
			Lost:        table.lost,
			Orientation: models.Orientation(table.orientation),
		}
		actual := robot.String()

		if actual != table.expected {
			t.Errorf("String was incorrect, expected %v, actual %v",
				table.expected,
				actual,
			)
		}
	}
}
